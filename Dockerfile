FROM mysql:5.7
# ROOT PASSWORD
ENV MYSQL_ROOT_PASSWORD=binpass
ENV MYSQL_USER=cicada3301
ENV MYSQL_PASSWORD=binpass

ENV MYSQL_DATA_DIR=/var/lib/mysql \
    MYSQL_RUN_DIR=/run/mysqld \
    MYSQL_LOG_DIR=/var/log/mysql
ADD init-development.sql /docker-entrypoint-initdb.d
ADD ["alldb.sql", "/tmp/dump.sql"]
ADD ./config_db.sh /config_db.sh
ADD ./config_db2.sh /config_db2.sh
ADD ./init_db.sh /tmp/init_db.sh

RUN chmod +x /config_db.sh
RUN chmod +x /config_db2.sh
RUN chmod +x /tmp/init_db.sh
#RUN /etc/init.d/mysql start && /config_db.sh && /etc/init.d/mysql stop
#RUN /etc/init.d/mysql start && /config_db2.sh && /etc/init.d/mysql stop
#CMD /etc/init.d/mysql start && mysql
#RUN /bin/bash -c "/usr/bin/mysqld_safe --skip-grant-tables &" && \
  #sleep 5 && \
 # mysql -u root -e "CREATE DATABASE all_bin" && \
 # mysql -u root all_bin < /tmp/dump.sql
#CMD mysql -u root -pbinpass all_bin < /tmp/dump.sql
RUN /etc/init.d/mysql start && \
        sleep 5 && \
		echo "ok"
RUN /tmp/init_db.sh

EXPOSE 3306
#docker exec -i sqlall -uroot -pnew-db-password --force < /tmp/dump.sql
#docker pull s3ks3k/r3v0luti0n-sql && docker run -d -p 3306:3306 --name sqlall0 -e MYSQL_ROOT_PASSWORD=binpass s3ks3k/r3v0luti0n-sql